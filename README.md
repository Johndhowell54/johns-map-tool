
A tool to let me create dungeon maps in table top role playing games

# Requirements
SDL2: https://www.libsdl.org/index.php

# Usage
The program is designed specifically to be used in
conjunctions with Emacs, something to sketch the dungeon and allow me
to write the details in an Emacs window. An example of this is below

![An example of use](./example.png)

# Controls

Mouse wheel up and down to select tiles (I haven't
implemented displaying which tile you currently are placing yet)

S key to save as a .bmp that is todays date. Haven't implemented
custom naming on save. Will probably have to embedd SDL2 in GTK to get
that sort of functionality.

Click on a placed tile to remove it, or click and drag to move it
around.

# Compiling
If the binary supplied doesn't work for you for whatever reason here's some instructions

### Linux
git clone https://gitlab.com/Johndhowell54/johns-map-tool
cd johns-map-tool
make

### Mac
Should be same as above but not sure, haven't tested

### Windows
No clue, sorry. If someone wants to make instructions feel free to submit a PR

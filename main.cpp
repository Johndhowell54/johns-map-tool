#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <list>
#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <ctime>

using namespace std;

// rectangles.remove(rect) causes the compiler to freak out if this isn't here
// It has something to do with SDL being a C lib, and list auto loop freaking it out
inline bool operator==(const SDL_Rect& a, const SDL_Rect& b)
{
    return a.x==b.x && a.y==b.y && a.w==b.w && a.h==b.h;
}

// This is so we can print the rect struct into a file
ostream &operator<<(ostream &os, const SDL_Rect &rect)
{
  return os << rect.x << endl
	    << rect.y << endl
	    << rect.w << endl
	    << rect.h << endl;
}

struct message
{
  char const *roomCon;
  SDL_Surface *surfaceMsg;// = TTF_RenderText_Solid(Sans, roomCon, black);
  SDL_Texture *message; //= SDL_CreateTextureFromSurface(renderer, surfaceMsg);
  SDL_Rect message_rect;
};

inline bool operator==(const message &a, const message &b)
{
 return a.roomCon==b.roomCon && a.surfaceMsg==b.surfaceMsg && a.message==b.message &&
   a.message_rect.x==b.message_rect.x && a.message_rect.y==b.message_rect.y && a.message_rect.w==b.message_rect.w &&
   a.message_rect.h==b.message_rect.h;

}

// Save our maps to a text file
void saveMap(string filename, list<SDL_Rect> &slist)
{
  fstream file_out;

  file_out.open(filename, ios_base::out);

  if (!file_out.is_open())
    {
      cout << "Failed to open " << filename << "\n";
    }
  else
    {
      for (auto item : slist)
	{
	  file_out << item << "\n";
	}
    }
  
}

void screenShot(string filename, SDL_Renderer *renderer, int w, int h)
{
  //SDL_SaveBMP needs a c style string
  char const *fname = filename.c_str();

  SDL_Surface *sshot = SDL_CreateRGBSurface(0, w, h, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
  SDL_RenderReadPixels(renderer, NULL, SDL_PIXELFORMAT_ARGB8888, sshot->pixels, sshot->pitch);
  //SDL_SaveBMP(sshot, fname);
  IMG_SavePNG(sshot, fname);
  SDL_FreeSurface(sshot);

}
int main(int argc, char** args)
{
  // Vars for the SDL event loop
  bool quit = false;
  SDL_Event event;

  // Tile and grid vars
  int tile_size = 28; //36;
  int tile_w = 29;
  int tile_h = 23;
  SDL_Color grid_line_color = {44, 44, 44, 255}; 
  
  // Window vars, +1 to get all of the grid lines on screen
  int win_w = (tile_w * tile_size) + 1;
  int win_h = (tile_h * tile_size) + 1;

  // Dragging and dropping vars
  bool leftMouseButtonDown = false;
  SDL_Point mousePos;

  // Initialize SDL and all its components
  SDL_Init(SDL_INIT_EVERYTHING);
  TTF_Init();
  
  // Create the window
  SDL_Window* window = SDL_CreateWindow("John's Mapping Tool", SDL_WINDOWPOS_UNDEFINED,
					SDL_WINDOWPOS_UNDEFINED, win_w, win_h, SDL_WINDOW_SHOWN);

  // Create a renderer for the window above, using the first rendering driver
  SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);


  // Lists to hold our SDL_Rect tiles
  list<SDL_Rect> wall_tiles;
  list<SDL_Rect> door_tiles;

  // Vars to hold our current selection for drag and drop, and which tiles to place
  SDL_Rect * selectedRect = NULL;
  string placeTile = "num";
  int currentTileSel = 0;
  int tileSelMax = 2;

  // Vars to place text/numbers for labeling
  SDL_Color black = {3,3,3};
  TTF_Font *Sans = TTF_OpenFont("inconsolata.ttf",14);
  int roomNum = 1; 
  string roomStr = to_string(roomNum);
  //char const *roomCon = roomStr.c_str();
  //SDL_Surface *surfaceMsg = TTF_RenderText_Solid(Sans, roomCon, black);
  //SDL_Texture *message = SDL_CreateTextureFromSurface(renderer, surfaceMsg);
  //SDL_Rect message_rect = {50,50, 50, 50};
  list<message> messages;


  while (!quit)
    {
      SDL_Delay(10);
      SDL_PollEvent(&event);

      //Sets the tile place selection
      switch(currentTileSel)
	{
	case 0:
	  placeTile = "wall";
	  break;
	case 1:
	  placeTile = "door";
	  break;

	case 2:
	  placeTile = "num";
	  break;

	}

      switch(event.type)
	{
	  case SDL_QUIT:
	    quit = true;
	    break;

	// Constantly updates our mouse position when it moves 
	case SDL_MOUSEMOTION:
	  mousePos = {event.motion.x, event.motion.y};

	  if (leftMouseButtonDown && selectedRect != NULL)
	    {
	      
	      selectedRect->x = mousePos.x;// - clickOffset.x;
	      selectedRect->y = mousePos.y;// - clickOffset.y;
	    }
	  break;

	// Handles mouse button release events
	case SDL_MOUSEBUTTONUP:
	  // Place our tile on left button release if we are dragging one
	  if (leftMouseButtonDown && event.button.button == SDL_BUTTON_LEFT)
	    {
	      leftMouseButtonDown = false;

	      if (selectedRect != NULL)
	      {
		selectedRect->x = ((mousePos.x / tile_size) * tile_size); 
	      
		selectedRect->y = ((mousePos.y / tile_size) * tile_size);

		selectedRect = NULL;
	      }
	      // Creates a new tile based on placeTile we set earlier if we are clicked on a blank tile
	      else
		{
		  
		  if (placeTile.compare("wall") == 0)
		    {
		      const SDL_Rect new_wall_tile = {((mousePos.x / tile_size) * tile_size), ((mousePos.y / tile_size) * tile_size), tile_size, tile_size};

		      wall_tiles.push_back(new_wall_tile);
		      break;
		    }
		  else if (placeTile.compare("door") == 0)
		    {
		      const SDL_Rect new_wall_tile = {((mousePos.x / tile_size) * tile_size), ((mousePos.y / tile_size) * tile_size), tile_size, tile_size};

		      door_tiles.push_back(new_wall_tile);
		      break;
		    }
		  else if (placeTile.compare("num") == 0)
		    {
			  string roomStr = to_string(roomNum);
			  const char *roomCon = roomStr.c_str();
			  SDL_Surface *surfaceMsg = TTF_RenderText_Solid(Sans, roomCon, black);
			  //message = SDL_CreateTextureFromSurface(renderer, surfaceMsg);

			  //message_rect = {((mousePos.x / tile_size) * tile_size), ((mousePos.y / tile_size) * tile_size),tile_size * 2,tile_size * 2};
			  message msg = { roomCon,
			  surfaceMsg,
			  SDL_CreateTextureFromSurface(renderer,surfaceMsg),
			  {((mousePos.x / tile_size) * tile_size),
			  ((mousePos.y / tile_size) *
			   tile_size),tile_size * 2,tile_size * 2}};
			  //SDL_RenderCopy(renderer, message, NULL, &message_rect);

			  messages.push_back(msg);
			  roomNum++;
			  break;

		    }
		}
	    }
	  break;

	  // Handle mouse button events
	case SDL_MOUSEBUTTONDOWN:
	  // Select a tile to drag and drog with left click
	  if (!leftMouseButtonDown && event.button.button == SDL_BUTTON_LEFT)
	    {
	      leftMouseButtonDown = true;
	      for (auto &rect : wall_tiles)
		{
		  if (SDL_PointInRect(&mousePos, &rect))
		    {
		      selectedRect = &rect;
		      break;
		    }
		}
	      for (auto &rect : door_tiles)
		{
		  if (SDL_PointInRect(&mousePos, &rect))
		    {
		      selectedRect = &rect;
		      break;
		    }
		}
	      for (auto msg : messages)
		{
		  //SDL_Rect msgr = &msg.message_rect;
		  if (SDL_PointInRect(&mousePos,&msg.message_rect))
		    {
		      selectedRect = &msg.message_rect;
		      messages.remove(msg);
		      roomNum--;
		      break;
		    }
		    break; 
		}
	     break;
	    }

	  // Delete the tile that the mouse is on 
	case SDL_BUTTON_RIGHT:
	    {
	      for (auto rect : wall_tiles)
		{
		  if (SDL_PointInRect(&mousePos, &rect))
		     {
		       wall_tiles.remove(rect);
		       break;
		     }
		}
	      for (auto rect : door_tiles)
		{
		  if (SDL_PointInRect(&mousePos, &rect))
		     {
		       door_tiles.remove(rect);
		       break;
		     }
		}

	      break;
	    }

	  // Scroll to change between tiles to place
	case SDL_MOUSEWHEEL:
	    {
	      // Scroll up
	      if (event.wheel.y > 0)
		{
		  if (currentTileSel != tileSelMax)
		    {
		      currentTileSel++;
		      break;
		    }
		  else
		    {
		      currentTileSel = 0;
		      break;
		    }
		  break;
		}
	      // Scroll down
	      if (event.wheel.y < 0)
		{
		  if (currentTileSel == 0)
		    {
		      break;
		    }
		  else
		    {
		      currentTileSel -= 1;
		      break;
		    }
		  break;
		}
	      

	    }

	    // Keys are triggered on keyup
	case SDL_KEYUP:
	  {
	    // Press 's' key to save your map to a .bmp pic
	    if (event.key.keysym.sym == SDLK_s)
	      {
		time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		char date[100] = {0};

		strftime(date, sizeof(date), "%Y-%m-%d", std::localtime(&now));
		string ext = ".png";
		string fname = date + ext;
		screenShot(fname,renderer, win_h, win_w);
		//SDL_Surface *image = SDL_LoadBMP(fname.c_str());
		//IMG_SavePNG(image,"test.png");
		break;
	      }
	    // Press 'l' to load the recently saved map
	    else if (event.key.keysym.sym == SDLK_l)
	      {
		placeTile = "num";
		break;
	      }

	  }

	  break;
	}

      // Set our background color
      SDL_SetRenderDrawColor(renderer, 242,242,242,255);
      SDL_RenderClear(renderer);


      // Draw our grid
      SDL_SetRenderDrawColor(renderer, grid_line_color.r, grid_line_color.g, grid_line_color.b,
			     grid_line_color.a);
      for (int x = 0; x < 1 + tile_w * tile_size;
	   x += tile_size)
	{
	  SDL_RenderDrawLine(renderer, x, 0, x, win_h);
	}

      for (int y = 0; y < 1 + tile_h * tile_size;
	   y += tile_size)
	{
	  SDL_RenderDrawLine(renderer, 0, y, win_w, y);
	}

      
      // Draw our tiles
      for (auto const& rect : wall_tiles)
	{
	  SDL_SetRenderDrawColor(renderer, 3, 3, 3, 255);

	  SDL_RenderFillRect(renderer, &rect);
	}
      for (auto const& rect : door_tiles)
	{
	  SDL_SetRenderDrawColor(renderer, 139, 69, 19, 255);

	  SDL_RenderFillRect(renderer, &rect);
	}

       for (auto const msg : messages)
	{
	  SDL_RenderCopy(renderer, msg.message, NULL, &msg.message_rect);
	}
       //SDL_RenderCopy(renderer, message, NULL, &message_rect);
      SDL_RenderPresent(renderer);
    }

 // Cleanup and quit SDL
  //SDL_FreeSurface(surfaceMsg);
  //SDL_DestroyTexture(message);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();

    return 0;
}
 
